import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'dart:convert';

class WorldTime {
  /// Location name for the UI
  String location;

  /// The time in the given location
  String time;

  /// URL to an asset flag icon
  String flag;

  /// Location URL for API endpoint
  String url;

  /// true or false if daytime or not
  bool isDaytime;

  /// WorldTime API
  WorldTime({this.location, this.flag, this.url});

  Future<void> getTime() async {
    try {
      // Make request
      String _url = 'http://worldtimeapi.org/api/timezone/$url';
      Response response = await get(_url);
      Map data = jsonDecode(response.body);

      // Get properties from data
      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(0, 3);

      // Create a DateTime object
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      // set the time property
      isDaytime = now.hour >= 6 && now.hour < 20;
      time = DateFormat.Hms().format(now);
    } catch (e) {
      print('caught error: $e');
      time = 'Could not get time data';
    }
  }
}
